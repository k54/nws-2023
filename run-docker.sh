#!/bin/sh

docker build -t nws-2023-app -f ./cmd/app/Dockerfile .
docker run --rm -it -p 3000:3000 nws-2023-app
