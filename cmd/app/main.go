package main

import (
	"log"

	"gitlab.com/k54/nws-2023/app"
)

// GET /123 => hello 123
func main() {
	implementation := app.Get()

	err := implementation.Listen(":3000")
	if err != nil {
		log.Fatal(err)
	}
}
