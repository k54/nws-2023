package main

import (
	"bytes"
	"encoding/json"
	"errors"
	"flag"
	"fmt"
	"io"
	"log"
	"net/http"
	"os"
	"strconv"
	"strings"
	"text/template"
)

func init() {
	log.SetFlags(log.LstdFlags | log.Lshortfile)
}

type RestCli struct {
	PrintFancy   bool
	AuthToken    string
	TemplateFile string
	Values       map[string]any
	Url          string
}

func (rc *RestCli) ParseArgs() {
	flag.BoolVar(&rc.PrintFancy, "f", false, "enable fancy output")
	flag.StringVar(&rc.AuthToken, "auth", "", "use auth token")
	flag.StringVar(&rc.TemplateFile, "template", "", "template file to use")
	rc.Values = map[string]any{}
	flag.Func(
		"value", "values to use in posted JSON",
		func(s string) error {
			splitted := strings.Split(s, ":")
			if len(splitted) != 2 {
				return errors.New("value must contain exactly one ':'")
			}
			key, value := splitted[0], splitted[1]
			valueFloat, err := strconv.ParseFloat(value, 64)
			if err != nil {
				rc.Values[key] = value
			} else {
				rc.Values[key] = valueFloat
			}
			return nil
		})

	flag.CommandLine.Parse(os.Args[2:])

	rc.Url = os.Args[1]
}

func (rc *RestCli) CreateRequest() (*http.Request, error) {

	request, err := http.NewRequest("GET", "https://"+rc.Url, nil)
	if rc.AuthToken != "" {
		request.Header.Set("Authorization", "Bearer "+rc.AuthToken)
	}

	var tpl *template.Template
	if rc.TemplateFile != "" {
		templateContents, err := os.ReadFile(rc.TemplateFile)
		if err != nil {
			return nil, err
		}
		tpl, err = template.New("json").
			Parse(string(templateContents))
		if err != nil {
			return nil, err
		}
	}

	if len(rc.Values) != 0 {
		buf := bytes.Buffer{}
		if tpl != nil {
			err = tpl.Execute(&buf, rc.Values)
		} else {
			err = json.NewEncoder(&buf).Encode(rc.Values)
		}
		if err != nil {
			return nil, err
		}
		request.Method = "POST"
		request.Header.Set("Content-Type", "application/json")
		request.Body = io.NopCloser(&buf)
	}

	return request, nil
}

func (rc *RestCli) MakeRequest(request *http.Request) error {

	response, err := http.DefaultClient.Do(request)
	if err != nil {
		return err
	}

	body, err := io.ReadAll(response.Body)
	if err != nil {
		return err
	}
	if rc.PrintFancy {
		var parsedBody any
		err := json.Unmarshal(body, &parsedBody)
		if err != nil {
			return err
		}
		encoder := json.NewEncoder(os.Stdout)
		encoder.SetIndent("", "  ")
		err = encoder.Encode(parsedBody)
		if err != nil {
			return err
		}
	} else {
		log.Printf("%s", body)
	}

	if response.StatusCode/100 != 2 {
		return fmt.Errorf(
			"Response status should be 2xx, got %d instead",
			response.StatusCode)
	}
	return nil
}

func main() {
	cli := &RestCli{}
	cli.ParseArgs()
	request, err := cli.CreateRequest()
	if err != nil {
		log.Fatal(err)
	}
	err = cli.MakeRequest(request)
	if err != nil {
		log.Fatal(err)
	}
}
