package app

import (
	"sync"
	"sync/atomic"
	"time"

	"github.com/gofiber/fiber/v2"
)

type Data struct {
	Value     string    `json:"value"`
	Reads     uint64    `json:"reads"`
	LastWrite time.Time `json:"lastWrite"`
}

func Get() *fiber.App {
	app := fiber.New(fiber.Config{Immutable: true})

	var mapLock sync.RWMutex
	store := map[string]*Data{
		"123": {
			Value:     "salut",
			LastWrite: time.Now(),
		},
	}

	app.Get("/:id", func(c *fiber.Ctx) error {
		mapLock.RLock()
		defer mapLock.RUnlock()
		value, ok := store[c.Params("id")]
		if !ok {
			return c.SendStatus(fiber.StatusNotFound)
		}
		atomic.AddUint64(&value.Reads, 1)
		return c.JSON(value)
	})

	app.Get("/:id/:value", func(c *fiber.Ctx) error {
		mapLock.Lock()
		defer mapLock.Unlock()
		store[c.Params("id")] = &Data{Value: c.Params("value"), LastWrite: time.Now()}
		return c.SendStatus(fiber.StatusCreated)
	})
	app.Post("/:id", func(c *fiber.Ctx) error {
		mapLock.Lock()
		defer mapLock.Unlock()
		store[c.Params("id")] = &Data{Value: string(c.Body()), LastWrite: time.Now()}
		return c.SendStatus(fiber.StatusCreated)
	})
	return app
}
