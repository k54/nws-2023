package app

import (
	"encoding/json"
	"io"
	"net/http/httptest"
	"testing"

	"github.com/gofiber/fiber/v2"
)

func TestGet(t *testing.T) {
	implementation := Get()
	{
		req := httptest.NewRequest("GET", "/123/chaton", nil)
		response, err := implementation.Test(req)
		if err != nil {
			t.Fatal(err)
		}
		t.Logf("%+v", response)

		if response.StatusCode != fiber.StatusCreated {
			t.Fatalf(
				"response status code was %v, should have been 201 Created",
				response.StatusCode)
		}
	}
	{
		req := httptest.NewRequest("GET", "/123", nil)
		response, err := implementation.Test(req)
		if err != nil {
			t.Fatal(err)
		}
		result, err := io.ReadAll(response.Body)
		t.Logf("%+v %s", response, result)
		var output Data
		err = json.Unmarshal(result, &output)
		if err != nil {
			t.Fatal(err)
		}
		if output.Value != "chaton" {
			t.Fatalf(`Output was %q, expected "chaton"`, output.Value)
		}

	}

}
