package app

import (
	"log"
	"testing"
)

type stats struct {
	calls int
}

func (s *stats) call1() {
	s.calls++
}
func (s stats) call2() {
	s.calls++
	log.Print(s)
}

func TestPointer(t *testing.T) {
	stat := &stats{}
	t.Log(stat.calls)
	stat.call1()
	t.Log(stat.calls)
	stat.call2()
	t.Log(stat.calls)

}
